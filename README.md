# MapThePaths Android app 

A companion app to MapThePaths (mapthepaths.org.uk).

The MapThePaths app allows users to find unsurveyed council paths while
out in the field and allows users to change the designation of OSM ways.

## IMPORTANT ! Currently semi-functional only

The app is currently only semi-functional as the OAuth 1 functionality from OSM has been removed, and OAuth 2 is now compulsory. It is quite likely this will be upgraded in the future; I will add it as an issue.
